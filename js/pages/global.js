'use strict';
import Swiper from 'swiper';

export const globalFunc = () => {
  scrollAnim();
  //
  form();
  menu();
  loader();
};

const homeSwiperInit = () => {

  const recentSwiper = new Swiper('.recent__works', {
    speed: 1200,
    slidesPerView: 1,
    navigation: {
      nextEl: '.recent__works .swiper-button-next',
      prevEl: '.recent__works .swiper-button-prev',
    },
    pagination: {
      el: '.recent__works .swiper-pagination',
      clickable: true,
    },
  });
  const videoSwiper = new Swiper('.gallery-videos .gallery__swiper', {
    speed: 400,
    slidesPerView: 1,
    loopedSlides: 3,
    loop: true,
    navigation: {
      nextEl: '.gallery-videos .swiper-button-next',
      prevEl: '.gallery-videos .swiper-button-prev',
    },
    pagination: {
      el: '.gallery-videos .swiper-pagination',
      clickable: true,
    },
  });
  const feedbackSwiper = new Swiper('.gallery-feedback .gallery__swiper', {
    speed: 400,
    slidesPerView: 1,
    navigation: {
      nextEl: '.gallery-feedback .swiper-button-next',
      prevEl: '.gallery-feedback .swiper-button-prev',
    },
    pagination: {
      el: '.gallery-feedback .swiper-pagination',
      clickable: true,
    },
  });
  const peoplesSwiper = new Swiper('.gallery-peoples .gallery__swiper', {
    speed: 400,
    slidesPerView: 4,
    navigation: {
      nextEl: '.gallery-peoples .swiper-button-next',
      prevEl: '.gallery-peoples .swiper-button-prev',
    },
    pagination: {
      el: '.gallery-peoples .swiper-pagination',
      clickable: true,
    },
  });
  const blogsSwiper = new Swiper('.gallery-blogs .gallery__swiper', {
    speed: 400,
    slidesPerView: 3,
    navigation: {
      nextEl: '.gallery-blogs .swiper-button-next',
      prevEl: '.gallery-blogs .swiper-button-prev',
    },
    pagination: {
      el: '.gallery-blogs .swiper-pagination',
      clickable: true,
    },
  });
  const tutorialSwiper = new Swiper('.tutorial__list .swiper-container', {
    speed: 400,
    // width: 230,
    slidesPerView: 5,
    centeredSlides: true,
    slideToClickedSlide: true,
    navigation: {
      nextEl: '.tutorial__list .swiper-button-next',
      prevEl: '.tutorial__list .swiper-button-prev',
    },
  });
};

const loader = () => {
  $(window).on('load', () => {
    let tS = 1000 - (Date.now() - window.__openTime);
    if (tS < 50) {
      tS = 50;
    }
    setTimeout(() => {
      $('body').addClass('loaded');
      scrollAnimEls($(window).scrollTop(), $(window).height());
      scrollHeader($(window).scrollTop(), $(window).height());
      homeSwiperInit();
      scrollBottomBtn();
    }, tS);
  });
};

const scrollBottomBtn = () => {
  $(document).on('click', '[data-type="scroll_bottom"]', function () {
    const $section = $(`[data-section="services"]`);
    const sectionT = $section.offset().top - 80;
    $('html, body').animate({scrollTop: sectionT}, 0);
    return false;
  });
};

const menu = () => {
  $(document).on('click','[data-menu="toggle"]', function () {
    $('#app').toggleClass('menu-open');
  })
};

const scrollAnimEls = (top, heigth) => {
  $('[data-anim]').each((i, el) => {
    const elTop = $(el).offset().top;
    const elH = $(el).innerHeight();
    if ((top + heigth*0.8) > elTop && (elTop + elH) > (top)) {
      $(el).attr('data-anim', 'true');
    } else {
      $(el).attr('data-anim', 'false');
    }
  })
};

const scrollAnim = () => {
  $(window).on('scroll', () => {
    const scrTop = $(window).scrollTop();
    const scrH = $(window).height();

    scrollAnimEls(scrTop, scrH);
    scrollHeader(scrTop, scrH);
  });
};

const scrollHeader = (scrTop, scrH) => {
  if(scrTop > 0) {
    if(!$('#app').hasClass('not-in-top')) {
      $('#app').addClass('not-in-top');
    }
  } else {
    if($('#app').hasClass('not-in-top')) {
      $('#app').removeClass('not-in-top');
    }
  }
};

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
const form = () => {
  $(document).on('input, blur', 'form[ajax-send] input, form[ajax-send] textarea', function (e) {
    const $inp = $(this);
    if ($inp.val() === '') {
      $inp.removeClass('not-empty');
    } else {
      $inp.addClass('not-empty');
      $inp.closest('.input-wrap').removeClass('error');
    }
    return false;
  });
  $(document).on('keyup', 'form[ajax-send] [name="phone"]', function (e) {
    const $inp = $(this);
    let val = $inp.val();
    let numbs = val.match(/\d/g) || '';
    if (numbs) {
      numbs = numbs.join('');
    }
    numbs = '+' + numbs;
    if (val !== numbs) {
      $inp.val(numbs);
    }
  });
  $(document).on('submit', 'form[ajax-send]', function (e) {
    const $form = $(this);
    const $inpName = $form.find('[name="name"]');
    const $inpPhone = $form.find('[name="phone"]');
    const $inpEmail = $form.find('[name="email"]');
    const $inpCompany = $form.find('[name="company"]');
    const $txtMessage = $form.find('[name="message"]');
    const $submit = $form.find('[type="submit"]');
    const $info = $form.find('.info-status');
    let err = false;

    if ($inpEmail.val() === '' || !validateEmail($inpEmail.val())) {
      $inpEmail.closest('.input-wrap').addClass('error');
      $inpEmail.focus();
      err = true;
    }
    if ($inpPhone.val() === '' || $inpPhone.val() === '+' || $inpPhone.val().length < 11) {
      $inpPhone.closest('.input-wrap').addClass('error');
      $inpPhone.focus();
      err = true;
    }
    if ($inpName.val() === '' || $inpName.val().length < 2) {
      $inpName.closest('.input-wrap').addClass('error');
      $inpName.focus();
      err = true;
    }
    if (!err) {
      $submit.attr('disabled', 'disabled');
      const to = $form.attr('action') || 'send.php';
      $.post(to, {
        name: $inpName.val(),
        phone: $inpPhone.val(),
        email: $inpEmail.val(),
        company: $inpCompany.val(),
        message: $txtMessage.val(),
      })
        .done((res) => {
          $info.find('.success').text('Success! Your message has been sent!');
          setTimeout(() => {
            $info.addClass('success');
            setTimeout(() => {
              $form.find('input,textarea').removeClass('not-empty').val('');
              $form.find('.input-wrap').removeClass('error');
              $info.removeClass('failure success');
              setTimeout(() => {
                $info.find('.success').text('');
                $('#modal [data-toggle-modal]').click();
              }, 800);
            }, 3500);
          }, 50);
        })
        .fail(() => {
          $info.find('.failure').text('Error! Your message has NOT been sent!');
          setTimeout(() => {
            $info.addClass('failure');
            setTimeout(() => {
              $info.removeClass('failure success');
              setTimeout(() => {
                $info.find('.failure').text('');
              }, 400);
            }, 3500);
          }, 50);
        })
        .always(() => {
          $submit.removeAttr('disabled');
        })
    }

    return false;
  });
};
