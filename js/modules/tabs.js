'use strict';

const tabsInit = () => {
  const $tabs = $('[data-tabs]');
  if (!$tabs.length) return;

  $tabs.find('[data-tab-btn]').each((i, el) => {
    const $btn = $(el);
    const type = $btn.data('tab-btn');

    if ($btn.hasClass('active')) {
      $tabs.find(`[data-tab-item="${type}"]`).fadeIn(500);
    } else {
      $tabs.find(`[data-tab-item="${type}"]`).fadeOut(0);
    }
  });

  $tabs.on('click', '[data-tab-btn]', function () {
    const $btn = $(this);
    const type = $btn.data('tab-btn');

    $tabs.find('[data-tab-btn]').removeClass('active');
    $btn.addClass('active');
    $tabs.find(`[data-tab-item]`).fadeOut(0, () => {
      $tabs.find(`[data-tab-item="${type}"]`).fadeIn(650);
      $(window).scrollTop($(window).scrollTop() + 1);
    });
  })
};

export const tabs = () => {
  tabsInit();
};
