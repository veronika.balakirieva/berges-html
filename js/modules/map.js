'use strict';

const markers = [];
let map = null;

const mapInit = () => {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: 46.4598865,
      lng: 30.5717043
    },
    zoom: 10
  });
  $('.addresses__item').each(function (i, el) {
      const lat = $(el).data('lat');
      const lng = $(el).data('lng');
      if (lat && lng) {
        markers[i] = new google.maps.Marker({
          position: {
            lat,
            lng
          },
          map
        });
      }
      $(el).click(() => {
        $('.addresses__item').removeClass('active');
        $(el).addClass('active');
        zoomToMarker(markers[i]);
      });
    });
};
const zoomToMarker = (marker) => {
  if (marker) {
    map.setZoom(13);
    map.panTo(marker.position);
  }
};


export const mapFunc = () => {
  mapInit();
};
