'use strict';

const videoInit = () => {
  const $video = $('[data-video]');
  const video_id = $('[data-video-id]').data('video-id') || null;
  if(!$video || $video.length < 1 || !video_id) return;

  const tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";

  const firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  let player;
  window.onYouTubeIframeAPIReady = function () {
    player = new YT.Player('videoPlayer', {
      height: '360',
      width: '640',
      videoId: video_id
    });
  }

  let done = false;
  function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
      setTimeout(stopVideo, 6000);
      done = true;
    }
  }
  function stopVideo() {
    player.stopVideo();
  }

  $('body').on('click', '[data-video-btn]', function (e) {
    e.preventDefault();

    $video.addClass('video-showed');
    player.playVideo();
  });
};

export const videoFunc = () => {
  videoInit();
};
