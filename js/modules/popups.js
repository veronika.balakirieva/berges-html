'use strict';

export const popups = (() => {
  const popupClass = 'b-popup';
  const showedClass = `${popupClass}--showed`;

  const eventListeners = () => {
    $(document).on('click', '[data-popup-open]', function () {
      const $btn = $(this);
      open($btn.attr('data-popup-open'));
    });
    $(document).on('click', '[data-popup-close]', function () {
      $('[data-popup]').each((i, el) => {
        $(el).removeClass(showedClass);
        $('body').removeClass('is-locked');
      });
    });
  };

  const open = (name) => {
    const $popup = $(`[data-popup="${name}"]`);
    if ($popup[0]) {
      $popup.addClass(showedClass);
      $('body').addClass('is-locked');
    }
  };

  const init = () => {
    eventListeners();
    console.log("popups init");
  };

  return {
    init
  };
})();
