// Load jQuery from NPM
import $ from 'jquery';
import {globalFunc} from './pages/global';
import {videoFunc} from './modules/video';
import {mapFunc} from './modules/map';
import {tabs} from './modules/tabs';
import {popups} from "./modules/popups";

import "lightgallery.js";
import "lg-zoom.js";
import "lg-pager.js";

window.jQuery = $;
window.$ = $;

const app = () => {
  globalFunc();
  videoFunc();
  // mapFunc();
  tabs();
  popups.init();

  lightGallery(document.getElementById('lightgallery'), {
    selector: '.items_list__item',
    thumbnail: false,
    cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
    counter: false,
    download: false,
    pager: true,
    actualSize: false,
  });

  $('.head__burger').on('click', function () {
    $(this).toggleClass('is-open');
  });

  $('.home__top__scroll').on('click', function () {
    $('html, body').animate({scrollTop: $('.section--recent').offset().top - 45}, 500)
  });
};

document.addEventListener('DOMContentLoaded', app);
